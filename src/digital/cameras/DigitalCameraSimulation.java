/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package digital.cameras;

/**
 *
 * @author mymac
 */
public class DigitalCameraSimulation {
    
    public static void main(String[] args) {
        
        PhoneCamera phone = new PhoneCamera("Apple","IPhone",6.0, 64);
        PointAndShootCamera point = new PointAndShootCamera("Canon","Powershot A590",8.0,16);
        
        System.out.println(point.display());
        System.out.println("-----------------------");
        System.out.println(phone.display());
    }
    
    
}
