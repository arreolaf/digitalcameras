/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package digital.cameras;

/**
 *
 * @author mymac
 */
public class PointAndShootCamera extends DigitalCamera  {
    
    PointAndShootCamera(){
        
    }
    PointAndShootCamera(String make, String model, double megapixels, int externalMemory){
        super(make,model,megapixels,externalMemory);
        
    }
     public String display(){
        return "The make of the Camera is a : "+ super.getMake() + "\nThe model is: " + super.getModel() + "\nThe megapixels are: " + super.getMegapixels() + "\nThe external memory is: " + super.getExternalMemory()+"GB";
    }
}
