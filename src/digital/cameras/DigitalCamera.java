/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package digital.cameras;

/**
 *
 * @author mymac
 */
public class DigitalCamera {

   private String make;
   private String model;
   private double megapixels;
   private int internalMemory;
   private int externalMemory;
   
   DigitalCamera(){
       
   }
   
   DigitalCamera(String make, String model, double megapixels,int externalMemory){
       this.make = make;
       this.model = model;
       this.megapixels = megapixels;
       this.externalMemory = externalMemory;
   }
   DigitalCamera(String make, int internalMemory, double megapixels,String model){
       this.make = make;
       this.internalMemory = internalMemory;
       this.megapixels = megapixels;
       this.model = model;
     
   }
    
   public String getMake(){
       return this.make;
   }
   
   public String getModel(){
       return this.model;
   }
   
   public double getMegapixels(){
       return megapixels;
   }
   
   public int getInternalMemory(){
       return internalMemory;
   }
   
   public int getExternalMemory(){
       return externalMemory;
   }
   
}
