/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package digital.cameras;

/**
 *
 * @author mymac
 */
public class PhoneCamera extends DigitalCamera {
    
    PhoneCamera(){
     
    }
    PhoneCamera(String make, String model, double megapixels, int internalMemory){
        super(make,model,megapixels,internalMemory);
    }
    
    public String display(){
        return "The make of the Phone is: "+ super.getMake() + "\nThe model is: " + super.getModel() + "\nThe megapixels are: " + super.getMegapixels() + "\nThe internal memory is: " + super.getInternalMemory()+"GB";
    }
}
